An implementation of a quiz game to find the year of some events
by chilowi@u-pem.fr

This project is cut in two parts:
- A library with useful classes to handle questions (and a sample of questions in the resources); a simple CLI to play is also present
- A Vert.x server distributing the questions
