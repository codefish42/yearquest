package fr.uge.yearquest.base;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/** A question with its right answer (a year) */
public class Question {
    private final String title;
    private final int answer;

    public Question(String title, int answer) {
        this.title = title;
        this.answer = answer;
    }

    public String getTitle() {
        return title;
    }

    public int getAnswer() {
        return answer;
    }

    @Override
    public String toString() {
        return title + "\t" + answer;
    }

    public int testAnswer(int answer) {
        if (this.answer < answer) return -1;
        else if (this.answer > answer) return 1;
        else return 0;
    }

    public static Question fromString(String s) {
        var elements = s.split("\t");
        try {
            return new Question(elements[0], Integer.parseInt(elements[1]));
        } catch (IndexOutOfBoundsException|NumberFormatException e) {
            // exception raised if there is not tab in the string or if the year is no an int
            throw new IllegalArgumentException("Cannot convert the string " + s + " to a question", e);
        }
    }

    public static List<Question> loadAllQuestions(String filepath) throws IOException {
        var result = new ArrayList<Question>();
        var is = Question.class.getResourceAsStream(filepath);
        try(var br = new BufferedReader(new InputStreamReader(is))) {
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                var line2 = line.strip();
                if (line2.length() > 0) {
                    try {
                        var q = fromString(line2);
                        result.add(q);
                    } catch (IllegalArgumentException e) {
                        System.err.println("Invalid line in file that cannot be interpreted as a question: " + line2);
                    }
                }
            }
        }
        return result;
    }
}
