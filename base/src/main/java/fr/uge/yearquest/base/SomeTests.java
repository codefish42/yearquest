package fr.uge.yearquest.base;

public class SomeTests {
    public static void main(String[] args) {
        String line = "En quelle année le langage Java a-t-il été créé\t1995";
        Question q = Question.fromString(line);
        System.out.println(q);
        System.out.println(q.toString().equals(line)); // true
    }
}
