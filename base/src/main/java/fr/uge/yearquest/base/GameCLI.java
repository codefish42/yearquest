package fr.uge.yearquest.base;

import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/** Year quiz implemented with a CLI interface */
public class GameCLI {
    public static final String QUESTIONS_PATH = "/qa.txt";

    private static final Random rng = new Random();

    public static <T> T pickRandomElement(List<T> list) {
        return list.get(rng.nextInt(list.size()));
    }

    public static void main(String[] args) throws IOException {
        var questions = Question.loadAllQuestions(QUESTIONS_PATH);
        var question = pickRandomElement(questions);
        try(Scanner sc = new Scanner(System.in)) {
            System.out.println(question.getTitle());
            while (sc.hasNextInt()) {
                var proposition = sc.nextInt();
                if (proposition == -1) {
                    System.out.println("You have given up; the answer is " + question.getAnswer());
                    return;
                } else {
                    int cmp = question.testAnswer(proposition);
                    switch(cmp) {
                        case -1: System.out.println("The answer is smaller"); break;
                        case 0: System.out.println("Good answer"); return;
                        case 1: System.out.println("The answer is greater"); break;
                    }
                }
            }
        }
    }
}
