package fr.uge.yearquest.server;

import fr.uge.yearquest.base.GameCLI;
import fr.uge.yearquest.base.Question;
import io.vertx.config.ConfigRetriever;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.sstore.LocalSessionStore;

import java.util.BitSet;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class MainVerticle extends AbstractVerticle {
  public static final String QUESTION_PATH = "/qa.txt";

  public static final Random RANDOM = new Random();

  @Override
  public void start(Promise<Void> startPromise) throws Exception {
    // load the questions
    var questions = Question.loadAllQuestions(QUESTION_PATH);
    var router = Router.router(vertx);
    var sessionStore = LocalSessionStore.create(vertx);
    var sessionHandler = SessionHandler.create(sessionStore);

    router.route().handler(sessionHandler);

    router.route(HttpMethod.GET, "/questionNumber").handler(req -> { req.response().putHeader("Content-Type", "text/plain").end("" + questions.size()); });

    router.route(HttpMethod.GET, "/question/:id").handler(req -> {
      var id = -1;
      try {
        id = Integer.parseInt(req.pathParam("id"));
      } catch (NumberFormatException e) { }
      if (id < 0 || id >= questions.size()) {
        req.response().setStatusCode(404).setStatusMessage("Not found").end("ID not valid");
      } else {
        var question = questions.get(id);
        req.response().putHeader("Content-Type", "text/plain; charset=UTF-8").end(question.toString());
      }
    });

    router.route(HttpMethod.GET, "/randomQuestion").handler(req -> {
      // a Set<Integer> could also be used
      BitSet alreadyProposed = req.session().get("alreadyProposedQuestions");
      if (alreadyProposed == null) {
        alreadyProposed = new BitSet(questions.size());
        req.session().put("alreadyProposedQuestions", alreadyProposed);
      }
      // if all the questions have been distributed
      if (alreadyProposed.cardinality() == questions.size())
        alreadyProposed.clear(); // we remove all the elements from the set

      var id = RANDOM.nextInt(questions.size());
      var id2 = alreadyProposed.nextClearBit(id);
      if (id2 == questions.size()) id2 = alreadyProposed.previousClearBit(id);
      alreadyProposed.set(id2);
      req.redirect("/question/" + id2);
    });

    vertx.createHttpServer().requestHandler(router).listen(8080, http -> {
      if (http.succeeded()) {
        startPromise.complete();
        System.out.println("HTTP server started on port 8080");
      } else {
        startPromise.fail(http.cause());
      }
    });
  }
}
